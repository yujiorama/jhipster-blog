/**
 * View Models used by Spring MVC REST controllers.
 */
package org.bitbucket.yujiorama.jhipster_blog.web.rest.vm;
