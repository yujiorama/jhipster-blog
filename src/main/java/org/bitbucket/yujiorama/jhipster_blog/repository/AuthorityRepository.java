package org.bitbucket.yujiorama.jhipster_blog.repository;

import org.bitbucket.yujiorama.jhipster_blog.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
