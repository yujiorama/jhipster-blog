package org.bitbucket.yujiorama.jhipster_blog;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("org.bitbucket.yujiorama.jhipster_blog");

        noClasses()
            .that()
                .resideInAnyPackage("org.bitbucket.yujiorama.jhipster_blog.service..")
            .or()
                .resideInAnyPackage("org.bitbucket.yujiorama.jhipster_blog.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..org.bitbucket.yujiorama.jhipster_blog.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
